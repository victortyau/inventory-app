import { Component, OnInit } from '@angular/core';
import { Product } from '../product.model';


@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {
  products: Product[];
    
  constructor() {
    this.products = [
        new Product('Programming', 'Linux Device Drivers', '/assets/images/gcc.jpg', ['Linux','GCC','Programming'], 100.00 ),
        new Product('Electronics', 'MicroControllers', '/assets/images/pic.png', ['Circuits', 'PCB', 'Design'], 170.00),
        new Product('Photography', 'Bokeh Shoot', '/assets/images/bokeh.jpg', ['Camera', 'Lens', 'Flashes'], 120.00),
    
    ];
  }

  ngOnInit(): void {
  }

}

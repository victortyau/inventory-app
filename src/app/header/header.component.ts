import { Component, OnInit } from '@angular/core';
import { faCoffee, faShoppingCart, faWineBottle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  faCoffee = faCoffee;
  faShoppingCart = faShoppingCart;
  faWineBottle = faWineBottle;
  
  constructor() { }

  ngOnInit(): void {
  }

}
